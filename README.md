# Ruby on Rails Tutorial sample application

This is the sample application for
[*Ruby on Rails Tutorial:
Learn Web Development with Rails*](http://www.railstutorial.org/)
by [Michael Hartl](http://www.michaelhartl.com/).

## License

All source code in the [Ruby on Rails Tutorial](http://railstutorial.org/)
is available jointly under the MIT License and the Beerware License. See
[LICENSE.md](LICENSE.md) for details.

## Getting started

To get started with the app, clone the repo and then install the needed gems:

```
$ bundle install --without production
```

Next, migrate the database:

```
$ rails db:migrate
```

Finally, run the test suite to verify that everything is working correctly:

```
$ rails test
```

If the test suite passes, you'll be ready to run the app in a local server:

```
$ rails server
```

For more information, see the
[*Ruby on Rails Tutorial* book](http://www.railstutorial.org/book).

[A&Q Chapter 4](#markdown-header-aq-chapter-4)

[A&Q Chapter 5](#markdown-header-aq-chapter-5)

[A&Q Chapter 6](#markdown-header-aq-chapter-6)

[A&Q Chapter 7](#markdown-header-aq-chapter-7)

[A&Q Chapter 8](#markdown-header-aq-chapter-8)

[A&Q Chapter 9](#markdown-header-aq-chapter-9)

[A&Q Chapter 10](#markdown-header-aq-chapter-10)

[A&Q Chapter 11](#markdown-header-aq-chapter-11)

[A&Q Chapter 12](#markdown-header-aq-chapter-12)

[A&Q Chapter 13](#markdown-header-aq-chapter-13)

[A&Q Chapter 14](#markdown-header-aq-chapter-14)

# A&Q Chapter 4
## 4.2.2
Q: What is the result if you replace double quotes with single quotes in the previous exercise?
```
A: É impresso todo o conteúdo da string sem fazer interpolação: #{city}\t#{state}
```


## 4.2.3
Q: What is the length of the string “racecar”?
```
A: "racecar".length => 7 (caracteres)
```

Q: What is the result of running the code shown in Listing 4.9? How does it change if you reassign the variable s to the string “onomatopoeia”?

```
A: 'puts "It's a palindrome!" if s == s.reverse' imprime "It's a palindrome!", atribuindo “onomatopoeia” a variável s nada é impresso no terminal.
```


## 4.3.1
Q: What is the result of selecting element 7 from the range of letters a through z? What about the same range reversed?
```
A: ('a'..'z').to_a[7] => "h"; ('a'..'z').to_a.reverse[7] => "s"
```


## 4.3.3
Q: Find an online version of the Ruby API and read about the Hash method merge. What is the value of the following expression?
'''
  { "a" => 100, "b" => 200 }.merge({ "b" => 300 })
'''
```
A: A função merge em um hash gera um novo hash, unido o hash que invoca com o que é passado como parâmetro. Se a mesma chave estiver na origem e no parâmentro o valor no parâmetro é mantido. No código de exemplo o novo hash gerado tem os valores: { "a" => 100, "b" => 300 }. O original não é alterado, para altera-lo é necessário usar "!".
```


## 4.4.1
Q: What is the literal constructor for the range of integers from 1 to 10?
```
A: (1..10)
```

Q: What is the constructor using the Range class and the new method?
```
A: Range.new(1, 10)
```


## 4.4.2
Q: What is the class hierarchy for a range? For a hash? For a symbol?
```
A: Range < Object < BasicObject; Hash < Object < BasicObject; Symbol < Object < BasicObject
```


## 4.4.3
Q: Verify that “racecar” is a palindrome and “onomatopoeia” is not. What about the name of the South Indian language “Malayalam”?
```
A: "Malayalam".palindrome? => false; "Malayalam".downcase.palindrome? => true
```


## 4.4.4
Q: Determine the class hierarchy of the user object.
```
A: User < ApplicationRecord(abstract) < ActiveRecord::Base < Object < BasicObject
```

# A&Q Chapter 5

## 5.1.3 Partials
Q: Because we haven’t yet created the partial needed by Listing 5.18, the tests should be red. Confirm that this is the case.
```
A: 5 tests, 0 assertions, 0 failures, 5 errors, 0 skips
```

Q: Create the necessary partial in the layouts directory, paste in the contents, and verify that the tests are now green again.
```
A: 5 tests, 9 assertions, 0 failures, 0 errors, 0 skips
```
## 5.3.2 Rails routes
Q: Confirm that the tests are now red. Get them to green by updating the route in Listing 5.28.
```
A: 4 tests, 6 assertions, 0 failures, 1 errors, 0 skips
```
## 5.3.4 Layout link tests
Q: In the footer partial, change about_path to contact_path and verify that the tests catch the error.
```
A: 1 tests, 4 assertions, 1 failures, 0 errors, 0 skips
```
## 5.4.1 Users controller
Q: The route in the previous exercise doesn’t yet exist, so confirm that the tests are now red.
```
A: 7 tests, 16 assertions, 0 failures, 1 errors, 0 skips
```
## 5.4.2 Signup URL
Q: In order to verify the correctness of the test in the previous exercise, comment out the signup route to get to red, then uncomment to get to green.
```
A: Comentado: 7 tests, 8 assertions, 0 failures, 3 errors, 0 skips |
Não comentado: 7 tests, 17 assertions, 0 failures, 0 errors, 0 skips
```

# A&Q Chapter 6

## 6.1.1 Database migrations
Q: Rails uses a file called schema.rb in the db/ directory to keep track of the structure of the database (called the schema, hence the filename). Examine your local copy of db/schema.rb and compare its contents to the migration code in Listing 6.2.
```
A: Dois campos adicionais: created_at e updated_at, eles não podem ser nulos.
```

Q: Most migrations (including all the ones in this tutorial) are reversible, which means we can “migrate down” and undo them with a single command, called db:rollback:
  $ rails db:rollback
After running this command, examine db/schema.rb to confirm that the rollback was successful.
```
A: ActiveRecord::Schema.define está com o bloco vazio.
```

Q: Re-run the migration by executing rails db:migrate again. Confirm that the contents of db/schema.rb have been restored.
```
A: Confirmado, está lá.
```
## 6.1.2 The model file
Q: In a Rails console, use the technique from Section 4.4.4 to confirm that User.new is of class User and inherits from ApplicationRecord.
```
A: User.new.class > User, User.new.class.superclass -> ApplicationRecord(abstract)
```

Q: Confirm that ApplicationRecord inherits from ActiveRecord::Base
```
A: User.new.class.superclass.superclass -> ActiveRecord::Base, ApplicationRecord.superclass ->ActiveRecord::Base
```
## 6.1.3 Creating user objects
Q: Confirm that user.name and user.email are of class String.
```
A: user.name.class -> String, user.email.class -> String
```

Q: Of what class are the created_at and updated_at attributes?
```
A: user.created_at.class -> ActiveSupport::TimeWithZone, user.updated_at.class -> ActiveSupport::TimeWithZone
```
## 6.1.4 Finding user objects
Q: Find the user by name. Confirm that find_by_name works as well. (You will often encounter this older style of find_by in legacy Rails applications.)
```
A: User.find_by(name: "doni") -> nil, User.find_by(name: "Doni") -> #<User id: 1, name: "Doni" ...> (case sensitive na busca), User.find_by_name("Doni") -> #<User id: 1, name: "Doni" ...>
```

Q: For most practical purposes, User.all acts like an array, but confirm that in fact it’s of class User::ActiveRecord_Relation.
```
A: User.all.class -> User::ActiveRecord_Relation
```

Q: Confirm that you can find the length of User.all by passing it the length method (Section 4.2.3). Ruby’s ability to manipulate objects based on how they act rather than on their formal class type is called duck typing, based on the aphorism that “If it looks like a duck, and it quacks like a duck, it’s probably a duck.”
```
A: User.all.length -> 2
```
## 6.1.5 Updating user objects
Q: Update the user’s name using assignment and a call to save.
```
A: user.email = "foo@bar.com", user.save -> true
```

Q: Update the user’s email address using a call to update_attributes.
```
A: user.update_attributes(email: "foo@bar.any") -> true
```

Q: Confirm that you can change the magic columns directly by updating the created_at column using assignment and a save. Use the value 1.year.ago, which is a Rails way to create a timestamp one year before the present time.
```
A: user.created_at = 1.year.ago, user.save -> true
```
## 6.2.1 A validity test
Q: In the console, confirm that a new user is currently valid.
```
A: User.new.valid? -> true
```

Q: Confirm that the user created in Section 6.1.3 is also valid
```
A: User.new(name: "Michael Hartl", email: "mhartl@example.com").valid? -> true
```
## 6.2.2 Validating presence
Q: Make a new user called u and confirm that it’s initially invalid. What are the full error messages?
```
A: u = User.new, u.valid? -> false
```

Q: Confirm that u.errors.messages is a hash of errors. How would you access just the email errors?
```
A: u.errors.messages[:email] -> ["can't be blank"]
```
## 6.2.3 Length validation
Q: Make a new user with too-long name and email and confirm that it’s not valid.
```
A: User.new(name: "a" * 100, email: "b" * 250 + "@test.com").valid? -> false
```

Q: What are the error messages generated by the length validation?
```
A: {:name=>["is too long (maximum is 50 characters)"], :email=>["is too long (maximum is 255 characters)"]}
```
## 6.3.2 User has secure password
Q: Confirm that a user with valid name and email still isn’t valid overall.
```
A: User.new(name: "Test", email: "test@test.co").valid? -> false
```

Q: What are the error messages for a user with no password?
```
A: u.errors.messages[:password] -> ["can't be blank"]
```
## 6.3.3 Minimum password standards
Q: Confirm that a user with valid name and email but a too-short password isn’t valid
```
A: User.new(name: "Test", email: "test@test.to", password: "secre", password_confirmation: "secre").valid? - > false
```

Q: What are the associated error messages?
```
A: u.errors.messages[:password] -> ["is too short (minimum is 6 characters)"]
```
## 6.3.4 Creating and authenticating a user
Q: Quit and restart the console, and then find the user created in this section.
```
A: User.find_by(name: "Michael Hartl")
```

Q: Try changing the name by assigning a new name and calling save. Why didn’t it work?

```
A: user.name = "Doni", user.save -> User Exists -> false (ele tenta criar um novo registro, mas o usuário "já existe")
```

Q: Update user’s name to use your name.
```
A: user.update_attribute(:name, "Doni") -> true
```

# A&Q Chapter 7

## 7.1.1 Debug and Rails environments
Q: Visit /about in your browser and use the debug information to determine the controller and action of the params hash.
```
A:  controller: static_pages, action: about
```

Q: In the Rails console, pull the first user out of the database and assign it to the variable user. What is the output of puts user.attributes.to_yaml? Compare this to using the y method via y user.attributes.

```
A: user.attributes.to_yaml os atributos são exibidos sem formatação (como se usasse o 'inspect'), enquanto y user.attributes exibe com formatação (quebra de linhas e tabulação).
```
## 7.1.2 A Users resource
Q: Using embedded Ruby, add Time.now to the user show page. What happens when you refresh the browser?
```
A: O tempo é atualizado.
```
## 7.1.3 Debugger
Q: With the debugger in the show action as in Listing 7.6, hit /users/1. Use puts to display the value of the YAML form of the params hash. Hint: Refer to the relevant exercise in Section 7.1.1.1. How does it compare to the debug information shown by the debug method in the site template?
```
A: A única diferença é onde a informação é impressa.
```

Q: Put the debugger in the User new action and hit /users/new. What is the value of @user?
```
A: nil
```
## 7.1.4 A Gravatar image and a sidebar
Q: Associate a Gravatar with your primary email address if you haven’t already. What is the MD5 hash associated with the image?
```
A: aeb19db739f7e2bdc0b4d5b383154b84
```

Q: The options hash used in the previous exercise is still commonly used, but as of Ruby 2.0 we can use keyword arguments instead. Confirm that the code in Listing 7.13 can be used in place of Listing 7.12. What are the diffs between the two?
```
A: Na forma antiga é necessário extrair as propriedades do hash, já nas keywords arguments se economiza código, digitando apenas as keywords.
```
## 7.2.1 Using form_for
Q: In Listing 7.15, replace :name with :nome. What error message do you get as a result?
```
A: undefined method 'nome' for #<User:0x007fe9bd3992a8>
```

Q: Confirm by replacing all occurrences of f with foobar that the name of the block variable is irrelevant as far as the result is concerned. Why might foobar nevertheless be a bad choice?
```
A: foobar não traz nenhum significado no contexto e só aumenta a quantidade de código. "f" é apenas um letra para digitar e facilmente associado a "form" (como a letra "i" indica index nos laços "for").
```
## 7.3.2 Strong parameters
Q: By hitting the URL /users/new?admin=1, confirm that the admin attribute appears in the params debug information.
```
A: admin: '1'
```
## 7.3.3 Signup error messages
Q: Confirm by changing the minimum length of passwords to 5 that the error message updates automatically as well.
```
A: Mensagem: Password is too short (minimum is 5 characters)
```

Q: How does the URL on the unsubmitted signup form (Figure 7.12) compare to the URL for a submitted signup form (Figure 7.18)? Why don’t they match?
```
A: /signup ou /users/new são as rotas para os formulários de cadastro, mas não são as rotas para o cadastro em si, que é feito via 'post' para /users
```
## 7.3.4 A test for invalid submission
Q: The URLs for an unsubmitted signup form and for a submitted signup form are /signup and /users, respectively, which don’t match. This is due to our use of a custom named route in the former case (added in Listing 5.43) and a default RESTful route in the latter case (Listing 7.3). Resolve this discrepancy by adding the code shown in Listing 7.26 and Listing 7.27. Submit the new form to confirm that both cases now use the same /signup URL. Are the tests still green? Why?
```
A: Porque a rota /users continua valida.
```
## 7.4.2 The flash
Q: In the console, confirm that you can use interpolation (Section 4.2.2) to interpolate a raw symbol. For example, what is the return value of "#{:success}"?
```
A: "#{:success}" => "success"
```

Q: How does the previous exercise relate to the flash iteration shown in Listing 7.30?
```
A: Para cada iteração em "alert alert-<%= message_type %>" o simbolo será convertido em uma string e concatenado com o restante. Ex: se message_type for :error "alert alert-<%= message_type %>" irá se tornar "alert alert-<error>"
```
## 7.4.4 A test for valid submission
Q: Suppose we changed @user.save to false in Listing 7.28. How does this change verify that the assert_difference block is testing the right thing?
```
A: O bloco de assert_difference não encontrará problemas, pois ele só verifica se os dados foram salvos. O erro acontecerá em assert_template.
```

# A&Q Chapter 8

## 8.1.1 Sessions controller
Q: What is the difference between GET login_path and POST login_path?
```
A: GET login_path abre o formulário de login, POST login_path recebe os dados de login enviados pelo usuário através do GET login_path.
```

Q: By piping the results of rails routes to grep, list all the routes associated with the Users resource. Do the same for Sessions. How many routes does each resource have? Hint: Refer to the section on grep in Learn Enough Command Line to Be Dangerous.

A:
```
rails routes | grep users
   signup GET    /signup(.:format)         users#new
          POST   /signup(.:format)         users#create
    users GET    /users(.:format)          users#index
          POST   /users(.:format)          users#create
 new_user GET    /users/new(.:format)      users#new
edit_user GET    /users/:id/edit(.:format) users#edit
     user GET    /users/:id(.:format)      users#show
          PATCH  /users/:id(.:format)      users#update
          PUT    /users/:id(.:format)      users#update
          DELETE /users/:id(.:format)      users#destroy
```

## 8.1.2 Login form
Q: Submissions from the form defined in Listing 8.4 will be routed to the Session controller’s create action. How does Rails know to do this? Hint: Refer to Table 8.1 and the first line of Listing 8.5.
```
A: O formulário envia uma requisição POST para login e a declaração "post '/login',    to: 'sessions#create'" em routes.rb informa o controller e a função que devem ser usados.
```
## 8.1.3 Finding and authenticating a user
Q: Using the Rails console, confirm each of the values in Table 8.2. Start with user = nil, and then use user = User.first. Hint: To coerce the result to a boolean value, use the bang-bang trick from Section 4.2.3, as in !!(user && user.authenticate(’foobar’)).
```
A: !!(nil && User.first.authenticate("abc123")) -> false, !!(User.first && User.first.authenticate("nopass")) -> false, !!(User.first && User.first.authenticate("abc123")) - true
```
## 8.2.1 The log_in method
Q: Log in with a valid user and inspect your browser’s cookies. What is the value of the session content? Hint: If you don’t know how to view your browser’s cookies, Google for it (Box 1.1).
```
A: \_sample_app_session = amlWR2hQSGtSSE5XWnFoWmJHVlNOMGtPUXBEN0NhNWhERGhPcE16SGRSMzBQbzhqdmc2dk1LMFl5TVRWNThaSittQzlqOWZ0aXRlNnQvVDdMMkNYR3lraE1OcUlqRUg1NzVYelJSd0FZUHhsRVVyK2l2Qjg4MkZqY3c2Z1Y1M2loSDl6cHlyeGxLem1zMExBUTNZMEVNa0VDL1NFb0tneDFNQXhPZ1RPanpnPS0tN3BtcjNHREdoRHoxdVFFNmdnME92dz09
```

Q: What is the value of the Expires attribute from the previous exercise?
```
A: Session
```
## 8.2.2 Current user
Q: Confirm at the console that User.find_by(id: ...) returns nil when the corresponding user doesn’t exist.
```
A: User.find_by(id: 1) -> <User id: 1, name: "Rails Tutorial", email: "example@railstutorial.org"...
```

Q: In a Rails console, create a session hash with key :user_id. By following the steps in Listing 8.17, confirm that the ||= operator works as required.
```
A: \@current_user ||= User.find_by(id: session[:user_id]) -> nil,
current_user ||= User.find_by(id: session[:user_id]) -> Carrega o usuário com id 1, repetindo o comando o usuário 1 continua carregado.
```
## 8.2.5 Login upon signup
Q: Is the test suite red or green if you comment out the log_in line in Listing 8.25?
```
A: Red
```
## 8.3 Logging out
Q: Confirm in a browser that the “Log out” link causes the correct changes in the site layout. What is the correspondence between these changes and the final three steps in Listing 8.31?
```
A: Clicar em "Log out" apaga o user_id da sessão, e a expressão condicional no header faz com que seja exibido o link de login e não os outros. os assert_select verificam a presença destes links.
```

# A&Q Chapter 9

## 9.1.1 Remember token and digest
Q: In the console, assign user to the first user in the database, and verify by calling it directly that the remember method works. How do remember_token and remember_digest compare?
```
irb(main):001:0> u = User.first
  User Load (0.1ms)  SELECT  "users".* FROM "users" ORDER BY "users"."id" ASC LIMIT ?  [["LIMIT", 1]]
=> #<User id: 1, name: "Rails Tutorial", email: "example@railstutorial.org", created_at: "2017-04-06 02:40:58", updated_at: "2017-04-06 02:40:58", password_digest: "$2a$10$Tc0x8GuXjrdG2gcM4YRzLeMBgaNG3fqbVAJrI0Flwl5...", remember_digest: nil>
irb(main):002:0> u.remember
   (0.1ms)  begin transaction
  SQL (0.3ms)  UPDATE "users" SET "updated_at" = ?, "remember_digest" = ? WHERE "users"."id" = ?  [["updated_at", 2017-04-27 03:28:05 UTC], ["remember_digest", "$2a$10$ixjL0MKzquPeVAr3MlSrt.VccECWKWvHF4L76OM6kfJKYlviF8sbO"], ["id", 1]]
   (3.9ms)  commit transaction
=> true
irb(main):003:0> u.remember_token
=> "h8RBu1qpUgLDNQIxbHIjjg"
irb(main):004:0> u.remember_digest
=> "$2a$10$ixjL0MKzquPeVAr3MlSrt.VccECWKWvHF4L76OM6kfJKYlviF8sbO"
```


Q: In Listing 9.3, we defined the new token and digest class methods by explicitly prefixing them with User. This works fine and, because they are actually called using User.new_token and User.digest, it is probably the clearest way to define them. But there are two perhaps more idiomatically correct ways to define class methods, one slightly confusing and one extremely confusing. By running the test suite, verify that the implementations in Listing 9.4 (slightly confusing) and Listing 9.5 (extremely confusing) are correct. (Note that, in the context of Listing 9.4 and Listing 9.5, self is the User class, whereas the other uses of self in the User model refer to a user object instance. This is part of what makes them confusing.)

```
With self:
23 tests, 66 assertions, 0 failures, 0 errors, 0 skips
With class << self:
23 tests, 66 assertions, 0 failures, 0 errors, 0 skips
```

## 9.1.2 Login with remembering
Q: By finding the cookie in your local browser, verify that a remember token and encrypted user id are present after logging in.
```
remember_token: fUrZ975gqZXSoH-eAvoNZA
user_id: Mg%3D%3D--8f3b4d23e6bcc8c3aab24d094f88e71fd99e3922
```

Q: At the console, verify directly that the authenticated? method defined in Listing 9.6 works correctly.
```
irb(main):001:0> u = User.first
irb(main):002:0> u.remember
irb(main):003:0> t = u.remember_token
irb(main):004:0> u.authenticated? t
=> true
```

## 9.1.3 Forgetting users
Q: After logging out, verify that the corresponding cookies have been removed from your browser.
```
Apenas o _sample_app_session permanece.
```

## 9.1.4 Two subtle bugs
Q: Comment out the fix in Listing 9.16 and then verify that the first subtle bug is present by opening two logged-in tabs, logging out in one, and then clicking “Log out” link in the other.
```
undefined method `forget' for nil:NilClass

```

Q: Comment out the fix in Listing 9.19 and verify that the second subtle bug is present by logging out in one browser and closing and opening the second browser.
```
invalid hash
```

Q: Uncomment the fixes and confirm that the test suite goes from red to green.
```
24 tests, 67 assertions, 0 failures, 0 errors, 0 skips
```

## 9.2 “Remember me” checkbox
Q: By inspecting your browser’s cookies directly, verify that the “remember me” checkbox is having its intended effect.
```
remember_token e user_id só estão presente se remember me for marcado na hora do login.
```

Q: At the console, invent examples showing both possible behaviors of the ternary operator (Box 9.2).
```
irb(main):001:0> b = true
irb(main):002:0> t = b ? "verdadeiro" : "falso"
irb(main):003:0> puts t
verdadeiro
irb(main):004:0> b = false
irb(main):005:0> t = b ? "verdadeiro" : "falso"
irb(main):006:0> puts t
falso
```

## 9.3.2 Testing the remember branch
Q: Verify by removing the authenticated? expression in Listing 9.33 that the second test in Listing 9.31 fails, thereby confirming that it tests the right thing.
```
28 tests, 73 assertions, 1 failures, 0 errors, 0 skips
```

# A&Q Chapter 10

## 10.1.2 Unsuccessful edits
Q: Confirm by submitting various invalid combinations of username, email, and password that the edit form won’t accept invalid submissions.
```
Todas as validações de cadastro estão disponíveis na edição.
```
## 10.1.4 Successful edits (with TDD)
Q: What happens when you change the email address to one without an associated Gravatar?
```
A imagem muda para a padrão do gravatar.
```

## 10.2.1 Requiring logged-in users
Q: As noted above, by default before filters apply to every action in a controller, which in our cases is an error (requiring, e.g., that users log in to hit the signup page, which is absurd). By commenting out the only: hash in Listing 10.15, confirm that the test suite catches this error.
```
32 tests, 77 assertions, 3 failures, 0 errors, 0 skips
```

## 10.2.2 Requiring the right user
Q: Why is it important to protect both the edit and update actions?
```
Para que nenhum visitante ou outro usuário possa ver o formulário de edição ou alterar os dados do usuário.
```

Q: Which action could you more easily test in a browser?
```
edit, nele só precisa acessar o endereço.
```

## 10.2.3 Friendly forwarding
Q: Put a debugger (Section 7.1.3) in the Sessions controller’s new action, then log out and try to visit /users/1/edit. Confirm in the debugger that the value of session[:forwarding_url] is correct. What is the value of request.get? for the new action? (Sometimes the terminal can freeze up or act strangely when you’re using the debugger; use your technical sophistication (Box 1.1) to resolve any issues.)
```
(byebug) session[:forwarding_url] = "http://localhost:3000//users/1/edit"
request.get? = true

```

## 10.3.3 Pagination
Q: Confirm at the console that setting the page to nil pulls out the first page of users.
```
irb(main):001:0> User.paginate(page: nil)
  User Load (2.4ms)  SELECT  "users".* FROM "users" LIMIT ? OFFSET ?  [["LIMIT", 30], ["OFFSET", 0]]
=> #<ActiveRecord::Relation [#<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-05-13 02:47:08", updated_at: "2017-05-13 02:47:08", password_digest: "$2a$10$eBOVDYm5305PZpXgcpebWex44rH4Ay6V8T4rAlvnE07..." (restante omitido)
```

Q: What is the Ruby class of the pagination object? How does it compare to the class of User.all?
```
irb(main):001:0> User.all.class
=> User::ActiveRecord_Relation
irb(main):002:0> User.paginate(page: nil).class
=> User::ActiveRecord_Relation
Tanto o método "all" quanto "paginate" retornam um objeto "User::ActiveRecord_Relation"
```

## 10.3.4 Users index test
Q: By commenting out the pagination links in Listing 10.45, confirm that the test in Listing 10.48 goes red.
```
=> Expected at least 1 element matching "div.pagination", found 0..
```

Q: Confirm that commenting out only one of the calls to will_paginate leaves the tests green. How would you test for the presence of both sets of will_paginate links? Hint: Use a count from Table 5.2.
```
assert_select 'div.pagination', count: 2
=> Expected exactly 2 elements matching "div.pagination", found 1..
```

## 10.3.5 Partial refactoring
Q: Comment out the render line in Listing 10.52 and confirm that the resulting tests are red.
```
Expected at least 1 element matching "a[href="/users/14035331"]", found 0..
```

## 10.4.2 The destroy action
Q: As the admin user, destroy a few sample users through the web interface. What are the corresponding entries in the server log?
```
   (0.2ms)  begin transaction
  SQL (1.5ms)  DELETE FROM "users" WHERE "users"."id" = ?  [["id", 10]]
   (13.9ms)  commit transaction
```

## 10.4.3 User destroy tests
Q: By commenting out the admin user before filter in Listing 10.59, confirm that the tests go red.
```
"User.count" didn't change by 0.
        Expected: 34
          Actual: 33
```


# A&Q Chapter 11

## 11.1.1 Account activations controller
Q: Verify that the test suite is still green.
```
42 tests, 205 assertions, 0 failures, 0 errors, 0 skips
```

Q: Why does Table 11.2 list the \_url form of the named route instead of the \_path form? Hint: We’re going to use it in an email.
```
In a domain example.com, _path returns '/account_activation/<token>/edit' and _url returns 'example.com/account_activation/<token>/edit'. Unless the user are accessing his mail throug example.com, he will never activate his account with a _path.
```
## 11.1.2 Account activation data model
Q: Verify that the test suite is still green after the changes made in this section.
```
42 tests, 205 assertions, 0 failures, 0 errors, 0 skips
```

Q: By instantiating a User object in the console, confirm that calling the create_activation_digest method raises a NoMethodError due to its being a private method. What is the value of the user’s activation digest?
```
Loading development environment (Rails 5.0.2)
irb(main):001:0> u = User.first
irb(main):002:0> u.create_activation_digest
NoMethodError: private method `create_activation_digest' called for #<User:0x0000000415c058>
Did you mean?  restore_activation_digest!
irb(main):003:0> u.activation_digest
=> "$2a$10$Lfx4mukOHQ.d2rhEhcjrne9bbRHsc6l9Q/sjsAPeoKdxrDisrtc/K"
```

Q: In Listing 6.34, we saw that email downcasing can be written more simply as email.downcase! (without any assignment). Make this change to the downcase_email method in Listing 11.3 and verify by running the test suite that it works.
```
42 tests, 205 assertions, 0 failures, 0 errors, 0 skips
```

## 11.2.1 Mailer templates
Q: At the console, verify that the escape method in the CGI module escapes out the email address as shown in Listing 11.15. What is the escaped value of the string "Don’t panic!"?

```
irb(main):001:0> CGI.escape('foo@example.com')
=> "foo%40example.com"
irb(main):002:0> CGI.escape("Don't panic!")
=> "Don%27t+panic%21"
```

## 11.2.2 Email previews
Q: Preview the email templates in your browser. What do the Date fields read for your previews?
```
Sat, 13 May 2017 21:20:49 +0000
```
## 11.2.3 Email tests
Q: Verify that the full test suite is still green.
```
43 tests, 214 assertions, 0 failures, 0 errors, 0 skips
```

Q: Confirm that the test goes red if you remove the call to CGI.escape in Listing 11.20.
```
 FAIL["test_account_activation", UserMailerTest, 3.772937458001252]
 test_account_activation#UserMailerTest (3.77s)
        Expected /michael@example\.com/ to match # encoding: US-ASCII
```

## 11.2.4 Updating the Users create action

Q: Sign up as a new user and verify that you’re properly redirected. What is the content of the generated email in the server log? What is the value of the activation token?
```
Hi Donizety,

Welcome to the sample App! Click on the link below to activate your account:

https://localhost:3000/account_activations/_6ZTRMJfFgJRnbKH0Fvx5Q/edit?email=dbaltokoski%40gmail.com


----==_mimepart_5917bcf3cb9cb_495e185a29045971
Content-Type: text/html;
 charset=UTF-8
Content-Transfer-Encoding: 7bit

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
      /* Email styles need to be inline */
    </style>
  </head>

  <body>
    <h1>Sample App</h1>

<p>Hi Donizety,</p>

<p>
  Welcome to the Sample App! Click on the link below to activate your account:
</p>

<a href="https://localhost:3000/account_activations/_6ZTRMJfFgJRnbKH0Fvx5Q/edit?email=dbaltokoski%40gmail.com">Activate</a>

  </body>
</html>

Activation token: _6ZTRMJfFgJRnbKH0Fvx5Q
```
Q: Verify at the console that the new user has been created but that it is not yet activated.
```
irb(main):001:0> User.find_by(email: 'dbaltokoski@gmail.com').activated?
=> false
```

## 11.3.1 Generalizing the authenticated? method
Q: Create and remember new user at the console. What are the user’s remember and activation tokens? What are the corresponding digests?
```
irb(main):001:0> u = User.create(name: "I'm a Test User", email: "test@test.t", password: "abc123", password_confirmation: "abc123")
irb(main):002:0> u.remember
irb(main):003:0> u.remember_token
=> "3O1IoJkPki54-d2ewih4lw"
irb(main):004:0> u.activation_token
=> "Q4LtxyLgS-ollf_4zocEiA"
irb(main):005:0> u.remember_digest
=> "$2a$10$MQCax1dGenBguzoCu1yXve5dZmo4tDGd73.gEstBo1WYSkCFgpysu"
irb(main):006:0> u.activation_digest
=> "$2a$10$yZBMafb0Nakz6xG6xGlnt.wgIW0XsgY7TXzqKnK0/QYRr9L5Z17ES"
```

Q: Using the generalized authenticated? method from Listing 11.26, verify that the user is authenticated according to both the remember token and the activation token.
```
irb(main):007:0> u.authenticated?(:remember, u.remember_token)
=> true
irb(main):008:0> u.authenticated?(:activation, u.activation_token)
=> true
```

## 11.3.2 Activation edit action
Q: Paste in the URL from the email generated in Section 11.2.4. What is the activation token?
```
irb(main):009:0> u.activation_token
=> "Q4LtxyLgS-ollf_4zocEiA"
```

Q: Verify at the console that the User is authenticated according to the activation token in the URL from the previous exercise. Is the user now activated?
```
irb(main):010:0> u.reload.activated?
=> true
```

# A&Q Chapter 12
## 12.1.1 Password resets controller
Q: Verify that the test suite is still green.
```
44 tests, 222 assertions, 0 failures, 0 errors, 0 skips
```

Q: Why does Table 12.1 list the \_url form of the edit named route instead of the \_path form? Hint: The answer is the same as for the similar account activations exercise (Section 11.1.1.1).
```
_url is the complete path to resource (with hostname), where _path is the relative path to the resource (without hostname).
```

## 12.1.2 New password resets
Q: Why does the form_for in Listing 12.4 use :password_reset instead of @password_reset?
```
Because there is no password_reset model.
```

## 12.1.3 Password reset create action
Q: Submit a valid email address to the form shown in Figure 12.6. What error message do you get?
```
wrong number of arguments (given 1, expected 0)
```

Q: Confirm at the console that the user in the previous exercise has valid reset_digest and reset_sent_at attributes, despite the error. What are the attribute values?
```
["reset_digest", "$2a$10$gzNhx5gk9STRj0640l2myuR8rZJw91O3zz4GL5DQdGoCbVKCKUcHu"], ["updated_at", 2017-05-15 03:11:42 UTC]
```

## 12.2.1 Password reset mailer and templates
Q: Preview the email templates in your browser. What do the Date fields read for your previews?
```
Mon, 15 May 2017 03:28:39 +0000
```

Q: Submit a valid email address to the new password reset form. What is the content of the generated email in the server log?
```
Sent mail to test@test.t (11.4ms)
Date: Mon, 15 May 2017 00:26:20 -0300
From: noreply@example.com
To: test@test.t
Message-ID: <59191fdc44103_5be91fb26647777@doni-pc-manjaro-desktop.mail>
Subject: Password reset
Mime-Version: 1.0
Content-Type: multipart/alternative;
 boundary="--==_mimepart_59191fdc419af_5be91fb2664763f";
 charset=UTF-8
Content-Transfer-Encoding: 7bit


----==_mimepart_59191fdc419af_5be91fb2664763f
Content-Type: text/plain;
 charset=UTF-8
Content-Transfer-Encoding: 7bit

To reset your password click the link below:

https://localhost:3000/password_resets/BZk2EUmYqEqeUPJqQqNi3Q/edit?email=test%40test.t

This link will expire in two hours.

If you did not request your password to be reset, please ignore this email and
your password will stay as it is.


----==_mimepart_59191fdc419af_5be91fb2664763f
Content-Type: text/html;
 charset=UTF-8
Content-Transfer-Encoding: 7bit

<!DOCTYPE html>
<html>
  <head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <style>
      /* Email styles need to be inline */
    </style>
  </head>

  <body>
    <h1>Password reset</h1>

<p>To reset your password click the link below: </p>

<a href="https://localhost:3000/password_resets/BZk2EUmYqEqeUPJqQqNi3Q/edit?email=test%40test.t">Reset password</a>

<p>This link will expire in two hours. </p>

<p>
If you did not request your password to be reset, please ignore this email and
your password will stay as it is.
</p>

  </body>
</html>

----==_mimepart_59191fdc419af_5be91fb2664763f--
```

Q: At the console, find the user object corresponding to the email address from the previous exercise and verify that it has valid reset_digest and reset_sent_at attributes.
```
irb(main):001:0> u = User.find_by(email: "test@test.t")
irb(main):002:0> u.reset_digest
=> "$2a$10$sfQmmljGt5il.0cU6wieN./9O0ejdIBrO1WbKarC3Axj09zowiA8W"
irb(main):003:0> u.reset_sent_at
=> Mon, 15 May 2017 03:26:20 UTC +00:00
```

## 12.2.2 Email tests
Q: Run just the mailer tests. Are they green?
```
2 tests, 16 assertions, 0 failures, 0 errors, 0 skips
```

Q: Confirm that the test goes red if you remove the second call to CGI.escape in Listing 12.12.
```
2 tests, 16 assertions, 1 failures, 0 errors, 0 skips
```

## 12.3.1 Reset edit action
Q: Follow the link in the email from the server log in Section 12.2.1.1. Does it properly render the form as shown in Figure 12.11?
```
Yes!
```

Q: What happens if you submit the form from the previous exercise?
```
Error: The action 'update' could not be found for PasswordResetsController
```

## 12.3.2 Updating the reset
Q: Follow the email link from Section 12.2.1.1 again and submit mismatched passwords to the form. What is the error message?
```
Password confirmation doesn't match Password
```

Q: In the console, find the user belonging to the email link, and retrieve the value of the password_digest attribute. Now submit valid matching passwords to the form shown in Figure 12.12. Did the submission appear to work? How did it affect the value of password_digest? Hint: Use user.reload to retrieve the new value.
```
irb(main):001:0> u = User.find_by(email: "test@test.t")
irb(main):002:0> u.password_digest
=> "$2a$10$hPtbTl3m4CNVNx27P0R7tOOKFFJuZjlVlyoeKuCInbR8aU5NBljiS"
irb(main):003:0> u.reload.password_digest
=> "$2a$10$G7z0AVJq6Dayk/DxcOzgUOWo1NalBEZLD4Hfz/orYw1gqzKCdnM8O"

It works. Changed the password(digest).
```

## 12.4 Email in production (take two)
Q: Sign up for a new account in production. Did you get the email?
```
Yes!
```

Q: Click on the link in the activation email to confirm that it works. What is the corresponding entry in the server log? Hint: Run heroku logs at the command line.
```
2017-05-15T04:40:02.946022+00:00 app[web.1]: D, [2017-05-15T04:40:02.945932 #10] DEBUG -- : [e35966e7-7e82-4cce-93ea-33538e4544a1]   SQL (1.6ms)  UPDATE "users" SET "password_digest" = $1, "updated_at" = $2 WHERE "users"."id" = $3  [["password_digest", "$2a$10$OT22M/AwLFjiAv1kVp.bKu7417JJI5j9gt0pjPd..Uj3TjxSCNyMa"], ["updated_at", 2017-05-15 04:40:02 UTC], ["id", 101]]
2017-05-15T04:40:02.956237+00:00 app[web.1]: D, [2017-05-15T04:40:02.956173 #10] DEBUG -- : [e35966e7-7e82-4cce-93ea-33538e4544a1]   SQL (1.5ms)  UPDATE "users" SET "reset_digest" = $1, "updated_at" = $2 WHERE "users"."id" = $3  [["reset_digest", nil], ["updated_at", 2017-05-15 04:40:02 UTC], ["id", 101]]

```

Q: Are you able to successfully update your password?
```
Yes!
```

# A&Q Chapter 13
## 13.1.1 The basic model
Q: Using Micropost.new in the console, instantiate a new Micropost object called micropost with content “Lorem ipsum” and user id equal to the id of the first user in the database. What are the values of the magic columns created_at and updated_at?
```
irb(main):001:0> m = Micropost.new(user: User.first, content: 'Lorem ipsum')
irb(main):002:0> m.created_at
=> nil
irb(main):003:0> m.updated_at
=> nil
```

Q: What is micropost.user for the micropost in the previous exercise? What about micropost.user.name?
```
irb(main):004:0> m.user
=> #<User id: 1, name: "Example User", email: "example@railstutorial.org", created_at: "2017-05-13 17:52:14", updated_at: "2017-05-13 17:52:14", password_digest: "$2a$10$URnhzg/kYkZk9MXOb..O0unvF3oqd.plaIrnYxoIYjR...", remember_digest: nil, admin: true, activation_digest: "$2a$10$Lfx4mukOHQ.d2rhEhcjrne9bbRHsc6l9Q/sjsAPeoKd...", activated: true, activated_at: "2017-05-13 17:52:14", reset_digest: nil, reset_sent_at: nil>
irb(main):005:0> m.user.name
=> "Example User"
```

Q: Save the micropost to the database. What are the values of the magic columns now?
```
irb(main):006:0> m.save!
irb(main):007:0> m.created_at
=> Tue, 16 May 2017 03:14:44 UTC +00:00
irb(main):008:0> m.updated_at
=> Tue, 16 May 2017 03:14:44 UTC +00:00
```

## 13.1.2 Micropost validations
Q: At the console, instantiate a micropost with no user id and blank content. Is it valid? What are the full error messages?
```
irb(main):001:0> m = Micropost.new
irb(main):002:0> m.valid?
=> false
irb(main):003:0> y m.errors
...
messages:
  :user:
  - must exist
  :user_id:
  - can't be blank
  :content:
  - can't be blank
details:
  :user:
  - :error: :blank
  :user_id:
  - :error: :blank
  :content:
  - :error: :blank
```

Q: At the console, instantiate a second micropost with no user id and content that’s too long. Is it valid? What are the full error messages?
```
irb(main):007:0> m = Micropost.new(content: "b" * 141)
irb(main):008:0> m.valid?
=> false
irb(main):009:0> y m.errors
...
messages:
  :user:
  - must exist
  :user_id:
  - can't be blank
  :content:
  - is too long (maximum is 140 characters)
details:
  :user:
  - :error: :blank
  :user_id:
  - :error: :blank
  :content:
  - :error: :too_long
    :count: 140
```

## 13.1.3 User/Micropost associations
Q: Set user to the first user in the database. What happens when you execute the command micropost = user.microposts.create(content: "Lorem ipsum")?
```
It creates a micrposost on database with "Lorem ipsum" as content and user_id equals to user.id
```

Q: The previous exercise should have created a micropost in the database. Confirm this by running user.microposts.find(micropost.id). What if you write micropost instead of micropost.id?
```
irb(main):007:0> user.micropost.find(micropost.id)
=> #<Micropost id: 2, content: "Lorem ipsum", user_id: 1, created_at: "2017-05-16 04:13:52", updated_at: "2017-05-16 04:13:52">
irb(main):008:0> user.micropost.find(micropost)
DEPRECATION WARNING: You are passing an instance of ActiveRecord::Base to `find`. Please pass the id of the object by calling `.id`. (called from irb_binding at (irb):8)
=> #<Micropost id: 2, content: "Lorem ipsum", user_id: 1, created_at: "2017-05-16 04:13:52", updated_at: "2017-05-16 04:13:52">
```

Q: What is the value of user == micropost.user? How about user.microposts.first == micropost?
```
irb(main):009:0> user == micropost.user
=> true
irb(main):010:0> user.micropost.first == micropost
=> false
(i have created a micropost for this user before, this is the second micropost, not the first)
```

## 13.1.4 Micropost refinements
Q: How does the value of Micropost.first.created_at compare to Micropost.last.created_at?
```
irb(main):001:0> Micropost.first.created_at
=> Tue, 16 May 2017 04:13:52 UTC +00:00
irb(main):002:0> Micropost.last.created_at
=> Tue, 16 May 2017 03:14:44 UTC +00:00

It is respected "ORDER DESC", where Micropost.first is the last created and Micropost.last is the first created.
```

Q: What are the SQL queries for Micropost.first and Micropost.last? Hint: They are printed out by the console.
```
irb(main):001:0> Micropost.first.created_at
SELECT  "microposts".* FROM "microposts" ORDER BY "microposts"."created_at" DESC LIMIT ?  [["LIMIT", 1]]
irb(main):002:0> Micropost.last.created_at
SELECT  "microposts".* FROM "microposts" ORDER BY "microposts"."created_at" ASC LIMIT ?  [["LIMIT", 1]]
```

Q: Let user be the first user in the database. What is the id of its first micropost? Destroy the first user in the database using the destroy method, then confirm using Micropost.find that the user’s first micropost was also destroyed.
```
irb(main):001:0> user = User.first
irb(main):002:0> user.micropost.first.id
=> 3
irb(main):003:0> user.micropost.first.destroy
  SQL (0.6ms)  DELETE FROM "microposts" WHERE "microposts"."id" = ?  [["id", 3]]
irb(main):004:0> Micropost.find 3
ActiveRecord::RecordNotFound: Couldn't find Micropost with 'id'=3
```

## 13.2.1 Rendering microposts
Q: As mentioned briefly in Section 7.3.3, helper methods like time_ago_in_words are available in the Rails console via the helper object. Using helper, apply time_ago_in_words to 3.weeks.ago and 6.months.ago.
```
irb(main):001:0> helper.time_ago_in_words 3.weeks.ago
=> "21 days"
irb(main):002:0> helper.time_ago_in_words 6.months.ago
=> "6 months"
```

Q: What is the result of helper.time_ago_in_words(1.year.ago)?
```
irb(main):003:0> helper.time_ago_in_words 1.year.ago
=> "about 1 year"
```

Q: What is the Ruby class for a page of microposts? Hint: Use the code in Listing 13.23 as your model, and call the class method on paginate with the argument page: nil.
```
irb(main):004:0> User.first.micropost.paginate(page: nil).class
=> Micropost::ActiveRecord_AssociationRelation
```

## 13.2.2 Sample microposts
Q: See if you can guess the result of running (1..10).to_a.take(6). Check at the console to see if your guess is right.
```
irb(main):001:0> (1..10).to_a.take(6)
=> [1, 2, 3, 4, 5, 6]
```

Q: Is the to_a method in the previous exercise necessary?
```
irb(main):001:0> (1..10).to_a.take(6)
=> [1, 2, 3, 4, 5, 6]
irb(main):002:0> (1..10).take(6)
=> [1, 2, 3, 4, 5, 6]
irb(main):003:0> (1..10).to_a.take(6) == (1..10).take(6)
=> true

.to_a is not necessary, is redundant.
```

Q: Faker has a huge number of occasionally amusing applications. By consulting the Faker documentation, learn how to print out a fake university name, a fake phone number, a fake Hipster Ipsum sentence, and a fake Chuck Norris fact.
```
irb(main):004:0> Faker::University.name
=> "Jakubowski Institute"
irb(main):005:0> Faker::PhoneNumber.phone_number
=> "(233) 717-0253 x419"
irb(main):006:0> Faker::Config.locale = 'pt-BR'
=> "pt-BR"
irb(main):007:0> Faker::PhoneNumber.phone_number
=> "(22) 56308-7283"
irb(main):008:0> Faker::Hipster.sentence
=> "Blog irony wayfarers flannel pitchfork polaroid +1."
irb(main):009:0> Faker::ChuckNorris.fact
=> "Chuck Norris rewrote the Google search engine from scratch."
```

## 13.2.3 Profile micropost tests
Q: Comment out the application code needed to change the two ’h1’ lines in Listing 13.28 from green to red.

```
<Michael Example> expected but was
        <>..
        Expected 0 to be >= 1.

54 tests, 260 assertions, 1 failures, 0 errors, 0 skips
```

## 13.3.1 Micropost access control
Q: Why is it a bad idea to leave a copy of logged_in_user in the Users controller?
```
One function will overwrite the other, and there is no guarantee of what will prevail. It will be difficult to find bugs.
```

## 13.3.3 A proto-feed
Q: Use the newly created micropost UI to create the first real micropost. What are the contents of the INSERT command in the server log?
```
INSERT INTO "microposts" ("content", "user_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["content", "outro teste"], ["user_id", 1], ["created_at", 2017-05-20 16:03:36 UTC], ["updated_at", 2017-05-20 16:03:36 UTC]]
```

Q: In the console, set user to the first user in the database. Confirm that the values of by Micropost.where("user_id = ?", user.id), user.microposts, and user.feed are all the same. Hint: It’s probably easiest to compare directly using ==.
```
irb(main):001:0> user = User.first
irb(main):002:0> Micropost.where("user_id = ?", user.id) == user.micropost
=> true
irb(main):003:0> user.micropost == user.feed
=> true
irb(main):004:0> Micropost.where("user_id = ?", user.id) == user.feed
=> true
```

## 13.3.4 Destroying microposts
Q: Create a new micropost and then delete it. What are the contents of the DELETE command in the server log?
```
DELETE FROM "microposts" WHERE "microposts"."id" = ?  [["id", 302]]
```

## 13.4.1 Basic image upload
Q: Upload a micropost with attached image. Does the result look too big? (If so, don’t worry; we’ll fix it in Section 13.4.3).
```
Yes, full sized (944x3218px)
```

## 13.4.2 Image validation
Q: What happens if you try uploading an image bigger than 5 megabytes?
```
The alert is displayed in browser (Maximum file size is 5MB. Please choose a smaller file) and an error from server (Picture should be less than 5MB ).
```

Q: What happens if you try uploading a file with an invalid extension?
```
Picture You are not allowed to upload "desktop" files, allowed types: jpg, jpeg, gif, png
```

## 13.4.3 Image resizing
Q: Upload a large image and confirm directly that the resizing is working. Does the resizing work even if the image isn’t square?
```
A non square image with 944x3218px gets scaled to 117x400px.
```

## 13.4.4 Image upload in production
Q: Upload a large image and confirm directly that the resizing is working in production. Does the resizing work even if the image isn’t square?
```
I don't know, Amazon dont let me add user. I tried to use my credential, no success. Heroku logs:
reason_phrase => "Forbidden"
status_line   => "HTTP/1.1 403 Forbidden\r\n"
status        => 403
```

# A&Q Chapter 14
## 14.1.1 A problem with the data model (and a solution)
Q: For the user with id equal to 1 from Figure 14.7, what would the value of user.following.map(&:id) be? (Recall the map(&:method_name) pattern from Section 4.3.2; user.following.map(&:id) just returns the array of ids.)
```
[2, 7, 10, 8]
```

Q By referring again to Figure 14.7, determine the ids of user.following for the user with id equal to 2. What would the value of user.following.map(&:id) be for this user?
```
[1]
```

## 14.1.2 User/relationship associations
Q: Using the create method from Table 14.1 in the console, create an active relationship for the first user in the database where the followed id is the second user.
```
irb(main):001:0> user = User.first
irb(main):002:0> user.active_relationships.create(followed_id: 2)
```

Q: Confirm that the values for active_relationship.followed and active_relationship.follower are correct.
```
irb(main):003:0> user.active_relationships.first.follower_id == user.id
=> true
irb(main):004:0> user.active_relationships.first.followed_id == 2
=> true
```

## 14.1.3 Relationship validations
Q: Verify by commenting out the validations in Listing 14.5 that the tests still pass. (This is a change as of Rails 5, and in previous versions of Rails the validations are required. We’ll plan to leave them in for completeness, but it’s worth bearing in mind that you may see these validations omitted in other people’s code.)
```
62 tests, 351 assertions, 0 failures, 0 errors, 0 skips
```

## 14.1.4 Followed users
Q: At the console, replicate the steps shown in Listing 14.9.
```
irb(main):001:0> user = User.first
irb(main):002:0> other = User.last
irb(main):003:0> user.following? other
=> false
irb(main):004:0> user.follow other
irb(main):005:0> user.following? other
=> true
irb(main):006:0> user.unfollow other
irb(main):007:0> user.following? other
=> false
```

Q: What is the SQL for each of the commands in the previous exercise?
```
SELECT  1 AS one FROM "users" INNER JOIN "relationships" ON "users"."id" = "relationships"."followed_id" WHERE "relationships"."follower_id" = ? AND "users"."id" = ? LIMIT ?  [["follower_id", 1], ["id", 100], ["LIMIT", 1]]

INSERT INTO "relationships" ("follower_id", "followed_id", "created_at", "updated_at") VALUES (?, ?, ?, ?)  [["follower_id", 1], ["followed_id", 100], ["created_at", 2017-05-22 04:19:28 UTC], ["updated_at", 2017-05-22 04:19:28 UTC]]

SELECT "users".* FROM "users" INNER JOIN "relationships" ON "users"."id" = "relationships"."followed_id" WHERE "relationships"."follower_id" = ?  [["follower_id", 1]]

DELETE FROM "relationships" WHERE "relationships"."follower_id" = ? AND "relationships"."followed_id" = 100  [["follower_id", 1]]

```

## 14.1.5 Followers
Q: At the console, create several followers for the first user in the database (which you should call user). What is the value of user.followers.map(&:id)?
```
irb(main):001:0> user = User.last
irb(main):002:0> user.followers << User.find(3, 5, 7, 11, 13)
irb(main):003:0> user.followers.map(&:id)
=> [3, 5, 7, 11, 13]
```

Q: Confirm that user.followers.count matches the number of followers you created in the previous exercise.
```
irb(main):004:0> user.followers.count
=> 5
irb(main):005:0> user.followers.count == [3, 5, 7, 11, 13].count
=> true
```

Q: What is the SQL used by user.followers.count? How is this different from user.followers.to_a.count? Hint: Suppose that the user had a million followers.
```
user.followers.count:  SELECT COUNT(*) FROM "users" INNER JOIN "relationships" ON "users"."id" = "relationships"."follower_id" WHERE "relationships"."followed_id" = ?  [["followed_id", 100]]
(It takes only one number from the database)

user.followers.to_a.count: read all records from database, put them on an array and cont each array item.
```

## 14.2.1 Sample following data
Q: Using the console, confirm that User.first.followers.count matches the value expected from Listing 14.14.
```
irb(main):001:0> User.first.followers.count == (3..40).size
=> true
```

Q: Confirm that User.first.following.count is correct as well.
```
irb(main):002:0> User.first.following.count == (2..50).size
=> true
```

## 14.2.2 Stats and a follow form
Q: Verify that /users/2 has a follow form and that /users/5 has an unfollow form. Is there a follow form on /users/1?
```
No.
```

Q: Write tests for the stats on the Home page. Hint: Add to the test in Listing 13.28. Why don’t we also have to test the stats on the profile page?
```
Becouse it uses the same partial.
```

## 14.2.3 Following and followers pages
Q: Verify in a browser that /users/1/followers and /users/1/following work. Do the image links in the sidebar work as well?
```
Everything works.
```

Q: Comment out the application code needed to turn the assert_select tests in Listing 14.29 red to confirm they’re testing the right thing.
```
Expected at least 1 element matching "a[href="/users/409608538"]", found 0..
67 tests, 366 assertions, 2 failures, 0 errors, 0 skips
```

## 14.2.4 A working follow button the standard way
Q: Follow and unfollow /users/2 through the web. Did it work?
```
Yes!
```

Q: According to the server log, which templates are rendered in each case?
```
Follow:
users/show.html.erb within layouts/application
shared/_stats.html.erb
users/_unfollow.html.erb
users/_follow_form.html.erb
collection of microposts/_micropost.html.erb
users/show.html.erb within layouts/application
layouts/_shim.html.erb
layouts/_rails_default.html.erb
layouts/_shim.html.erb
layouts/_header.html.erb
layouts/_footer.html.erb

Unfollow:
users/show.html.erb within layouts/application
shared/_stats.html.erb
users/_follow.html.erb
users/_follow_form.html.erb
collection of microposts/_micropost.html.erb
users/show.html.erb within layouts/application
layouts/_shim.html.erb
layouts/_rails_default.html.erb
layouts/_shim.html.erb
layouts/_header.html.erb
layouts/_footer.html.erb
```

## 14.2.5 A working follow button with Ajax
Q: Unfollow and refollow /users/2 through the web. Did it work?
```
Yes!
```

Q: According to the server log, which templates are rendered in each case?
```
Follow:
relationships/create.js.erb
users/_unfollow.html.erb

Unfollow:
relationships/destroy.js.erb
users/_follow.html.erb
```

## 14.2.6 Following tests
Q: By commenting and uncommenting each of the lines in the respond_to blocks (Listing 14.36), verify that the tests are testing the right things. Which test fails in each case?
```
ActionController::UnknownFormat: ActionController::UnknownFormat
test_should_unfollow_a_user_with_Ajax
test_should_follow_a_user_the_standard_way
```

Q: What happens if you delete one of the occurrences of xhr: true in Listing 14.40? Explain why this is a problem, and why the procedure in the previous exercise would catch it.
```
No errors. It tests without ajax.
```

## 14.3.1 Motivation and strategy
Q: Assuming the micropost’s ids are numbered sequentially, with larger numbers being more recent, what would user.feed.map(&:id) return for the feed shown in Figure 14.22? Hint: Recall the default scope from Section 13.1.4.

```
[10, 9, 7, 5, 4, 2, 1]
```

## 14.3.2 A first feed implementation
Q: In Listing 14.44, remove the part of the query that finds the user’s own posts. Which test in Listing 14.42 breaks?
```
test_feed_should_have_the_right_posts
  assert michael.feed.include?(post_self)
```

Q: In Listing 14.44, remove the part of the query that finds the followed users’ posts. Which test in Listing 14.42 breaks?
```
test_feed_should_have_the_right_posts
  assert michael.feed.include?(post_following)
```

Q: How could you change the query in Listing 14.44 to have the feed erroneously return microposts of unfollowed users, thereby breaking the third test in Listing 14.42? Hint: Returning all the microposts would do the trick.
```
Replacing the 'where(...)' with 'all'
test_feed_should_have_the_right_posts
  assert_not michael.feed.include?(post_unfollowed)
```

## 14.3.3 Subselects
Q: Note that Listing 14.49 escapes the expected HTML using CGI.escapeHTML (which is closely related to the CGI.escape method we used in Section 11.2.3 to escape URLs). Why is escaping the HTML necessary in this case? Hint: Try removing the escaping and carefully inspect the page source for the micropost content that doesn’t match. Using the search feature of your terminal shell (Cmd-F on Ctrl-F on most systems) to find the word “sorry” may prove particularly helpful.
```
Because it is escaped on server, and searching for special chars will not find anything.
```
