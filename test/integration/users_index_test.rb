require 'test_helper'

class UsersIndexTest < ActionDispatch::IntegrationTest

  def setup
    @admin = @user = users(:michael)
    @non_admin = users(:archer)
  end

  test "index including pagination" do
    log_in_as @user
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination', count: 2
    User.paginate(page: 1).each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
    end
  end

  test "index as admin including pagination and delete links" do
    log_in_as @admin
    get users_path
    assert_template 'users/index'
    assert_select 'div.pagination'
    first_page_of_users = User.paginate(page: 1)
    first_page_of_users.each do |user|
      assert_select 'a[href=?]', user_path(user), text: user.name
      unless user == @admin
        assert_select 'a[href=?]', user_path(user), text: 'delete'
      end
    end
    assert_difference 'User.count', -1 do
      delete user_path @non_admin
    end
  end

  test "index as non-admin" do
    log_in_as @non_admin
    get users_path
    assert_select 'a', text: 'delete', count: 0
  end

  test "should not show not activated users" do
      log_in_as @non_admin
      user = User.create(name: "Inactive User", email: "inactive@example.gov",
                        password: 'password', password_confirmation: 'password',
                        activated: false)
      get "#{users_path}?page=2"
      assert_select 'a[href=?]', user_path(user), text: user.name, count: 0

      get user_path(user)
      follow_redirect!
      assert_template 'static_pages/home'
  end

end
